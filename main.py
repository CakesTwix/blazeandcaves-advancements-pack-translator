import os
import json
import codecs
import re

folder = '/'.join(os.path.abspath(__file__).split('/')[:-1])
langs = [
    name[:-5] for name in os.listdir(f"{folder}/lang") if name.endswith('.json')
]
print("Выберите язык: ")
for i in range(len(langs)):
    print(f"{i}: {langs[i]}")
select_lang = int(input(" : "))

print(" : Парсинг перевода")
with codecs.open(f'{folder}/lang/{langs[select_lang]}.json', 'r', 'utf-8-sig') as file:
    json_parse = re.sub(r'#.+\n', '', file.read())  # Нафига в json комментарии?? Ток мешают парсить
    json_parsed = json.loads(json_parse)

print(" : Парсинг ачивок")
advancements_json = {}
for advancements_folder in ["blazeandcave","minecraft"]:
    list_dir_advancements = os.listdir(f"{folder}/bac_advancements/data/{advancements_folder}/advancements")
    for advancements_folder_item in list_dir_advancements:
        advancements_json[advancements_folder_item] = os.listdir(
            f"{folder}/bac_advancements/data/{advancements_folder}/advancements/{advancements_folder_item}")
        list_dir_advancements_json = os.listdir(
                f"{folder}/bac_advancements/data/{advancements_folder}/advancements/{advancements_folder_item}")
        for advancements_json__item in list_dir_advancements_json:
            open_json = f"{folder}/bac_advancements/data/{advancements_folder}/advancements/{advancements_folder_item}/{advancements_json__item}"
            with codecs.open(
                    open_json,
                    'r', 'utf-8-sig') as file_json:
                file_json_parsed = json.load(file_json)
                if 'display' in file_json_parsed:
                    if file_json_parsed["display"]["title"]["translate"] in json_parsed and file_json_parsed["display"]["description"]["translate"] in json_parsed:
                        file_json_parsed["display"]["title"]["translate"] = json_parsed[file_json_parsed["display"]["title"]["translate"]]
                        file_json_parsed["display"]["description"]["translate"] = json_parsed[file_json_parsed["display"]["description"]["translate"]]
                        
                        with open(open_json, 'w') as outfile:
                            json.dump(file_json_parsed, outfile)
