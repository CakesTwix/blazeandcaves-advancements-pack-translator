# BlazeandCaves Advancements Pack Translator

## Setup
1. [Download zip archive](https://www.planetminecraft.com/data-pack/blazeandcave-s-advancements-pack-1-12/)
- 1. Unzip near the script
- 2. Rename to "bac_advancements"
2. [Download translate zip](https://www.planetminecraft.com/texture-pack/bacap-language-pack/)
- 1. Unzip near the script 
- 2. Rename to "lang"
3. Run the python main.py command
